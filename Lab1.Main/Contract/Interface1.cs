﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Contract
{
    interface IMalpa
    {
        void Jedz();
        void Pij();
    }
    interface IGoryl : IMalpa
    {
        void Skacz();
    }
    interface IPawian : IMalpa
    {
        void Biegaj();
    }
    interface IOlbrzym
    {
        void Jedz();
        void Spij();
    }

}
