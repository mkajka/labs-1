﻿using Lab1.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    class Impl1 : IGoryl
    {
        public Impl1() { }

        public void Jedz()
        {
            Console.WriteLine("Am");
        }
        public void Pij()
        {
            Console.WriteLine("Gul");
        }
        public void Skacz()
    {
        Console.WriteLine("up");
    }

    }
    class Impl2 : IPawian
    {
        public Impl2() { }
        public void Jedz()
        {
            Console.WriteLine("Am");
        }
        public void Pij()
        {
            Console.WriteLine("Gul");
        }
        public void Biegaj()
        {
            Console.WriteLine("Run");
        }
    }
    class Impl3 : IGoryl, IOlbrzym
    {
        public Impl3()
        {
        }
        public void Jedz()
        {
            Console.WriteLine("Am");
        }
        public void Pij()
        {
            Console.WriteLine("Gul");
        }
        public void Skacz()
        {
            Console.WriteLine("up");
        }
        public void Spij()
        {
            Console.WriteLine("śpij");
        }
    
    }
}
