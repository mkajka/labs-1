﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(string);
        
        public static Type ISub1 = typeof(string);
        public static Type Impl1 = typeof(string);
        
        public static Type ISub2 = typeof(string);
        public static Type Impl2 = typeof(string);
        
        
        public static string baseMethod = "IMalpa";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "IGoryl";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "IPawian";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Lista";
        public static string collectionConsumerMethod = "Pij";

        #endregion

        #region P3

        public static Type IOther = typeof(string);
        public static Type Impl3 = typeof(string);

        public static string otherCommonMethod = "Jedz";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
